export function request(url, body, headers, method) {
  return fetch(`https://localhost:8443/idm/${url}`, {
    method: method,
    credentials: "include",
    headers: headers,
    body: JSON.stringify(body),
  })
    .then((response) =>
      response.json().then((data) => ({ status: response.status, body: data }))
    )
    .catch((error) => ({ error }));
}
