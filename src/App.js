import React from "react";
import { useDispatch } from "react-redux";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import HomeContext from "./components/HomeContext/HomeContext";
import HallOfFame from "./components/HallOfFame/HallOfFame";
import News from "./components/News/News";
import Dialog from "./components/Dialog/Dialog";
import Drawer from "./components/Drawer/Drawer";
import ConfirmRegistration from "./components/ConfirmRegistration/ConfirmRegistration";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { Switch, Route, Redirect } from "react-router-dom";
import ContainerStickyAlert from "./components/containerStickyAlert/containerStickyAlert";
import { SnackbarProvider } from "notistack";
import Cookies from "js-cookie";
import { isLoggedIn } from "./store/actions/sagaRequest.action";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#9cd8eb",
      main: "#74c1da",
      dark: "#468ca2",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

const App = () => {
  const dispatch = useDispatch();

  if (Cookies.get("idmToken")) {
    dispatch(isLoggedIn());
  }
  return (
    <div>
      <MuiThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={10}>
          <ContainerStickyAlert>
            <Header />
            <Switch>
              <Route exact path="/" component={HomeContext} />
              <Route exact path="/halloffame" component={HallOfFame} />
              <Route exact path="/news" component={News} />
              <Route
                path="/confirmregistration/:id"
                component={ConfirmRegistration}
              />
              <Route exact path="*">
                <Redirect to="/" />
              </Route>
            </Switch>
            <Footer />
            <Dialog />
            <Drawer />
          </ContainerStickyAlert>
        </SnackbarProvider>
      </MuiThemeProvider>
    </div>
  );
};

export default App;
