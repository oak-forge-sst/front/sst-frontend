import { useEffect, useCallback } from "react";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { confirmRegistration } from "../../store/actions/sagaRequest.action";

const ConfirmRegistration = () => {
  let { id } = useParams();
  let history = useHistory();
  const dispatch = useDispatch();
  const redirect = useCallback(
    () => dispatch(confirmRegistration(id, history)),
    [dispatch]
  );

  useEffect(() => {
    redirect(id, history);
  }, []);

  return null;
};

export default ConfirmRegistration;
