import React, { useCallback } from "react";
import Dialog from "@material-ui/core/Dialog";
import { useSelector, useDispatch } from "react-redux";
import { dialogClose } from "../../store/actions/dialog.action";


export default function FormDialog() {
  const isOpen = useSelector(state => state.dialogReducer.isOpen);
  const Component = useSelector(state => state.dialogReducer.component);
  const dispatch = useDispatch();

  const closeDialog = useCallback(() => dispatch(dialogClose()), [dispatch]);

  return (
    <Dialog
      open={isOpen}
      onClose={closeDialog}
      aria-labelledby="form-dialog-title"
    >
      <Component />
    </Dialog>
  );
}
