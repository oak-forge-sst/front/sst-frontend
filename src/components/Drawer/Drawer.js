import React, { useCallback } from "react";
import { useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import { useStyles } from "./DrawerStyle";
import { useSelector, useDispatch } from "react-redux";
import { drawerToggle } from "../../store/actions/drawer.action";
import Navigation from "../Navigation/Navigation.js";

export default function DrawerComp() {
  const drawerOpen = useSelector(state => state.drawerReducer);
  const dispatch = useDispatch();

  const toggleDrawer = useCallback(() => dispatch(drawerToggle()), [dispatch]);

  const classes = useStyles();
  const theme = useTheme();
  return (
    <Drawer
      className={classes.drawer}
      anchor="left"
      open={drawerOpen}
      onClose={toggleDrawer}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={toggleDrawer}>
          {theme.direction === "ltr" ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>
      <Divider />
      <List>
        <ListItem>
          <Navigation
            onClick={toggleDrawer}
            additionalClass={true}
            direction={"column"}
            justify={"flex-start"}
            alignItems={"flex-start"}
          />
        </ListItem>
      </List>
    </Drawer>
  );
}
