import React, { Component } from "react";
import { filterStyles } from "./FilterStyle";
import Button from "@material-ui/core/Button";
import TextInput from "../TextInput/TextInput";
import { validation } from "../Validation/Validation";
import FilterPanel from "../FilterPanel/FilterPanel";
import { withStyles } from "@material-ui/styles";

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      scoreFrom: "",
      scoreTo: "",
      membershipFrom: "",
      membershipTo: "",
      winsFrom: "",
      winsTo: "",
      scoreTouch: false,
      membershipTouch: false,
      winsTouch: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.changingLabel = this.changingLabel.bind(this);
    this.addingItemToLabel = this.addingItemToLabel.bind(this);
    this.addingItemToLabelName = this.addingItemToLabelName.bind(this);
  }

  handleChange(value, id, label) {
    this.setState({ [`${id}Touch`]: true });
    if (validation(value)) {
      this.changingLabel(value, id, label);
    } else {
      this.setState({
        [`${id}${label}`]: "",
      });
    }
  }

  changingLabel(value, id, label) {
    this.setState({
      [`${id}${label}`]: value,
    });
  }

  addingItemToLabel(firstLabel, secondLabel) {
    if (firstLabel && secondLabel) {
      return `(${firstLabel} - ${secondLabel})`;
    } else if (firstLabel) {
      return `(>${firstLabel})`;
    } else if (secondLabel) {
      return `(<${secondLabel})`;
    } else {
      return "";
    }
  }
  addingItemToLabelName(firstLabel) {
    if (firstLabel) {
      return `(${firstLabel})`;
    } else {
      return "";
    }
  }

  handleClick(e) {
    e.preventDefault();
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        {this.props.showFilter && (
          <Button
            className={classes.closeButton}
            onClick={this.props.showTable}
          >
            {" "}
            &#10799;{" "}
          </Button>
        )}
        <form className={classes.root} noValidate autoComplete="off">
          <div className={classes.filterRow}>
            <FilterPanel
              label={`Name ${this.addingItemToLabelName(this.state.name)}`}
              content={
                <div className={classes.formRow}>
                  <TextInput
                    id={"name"}
                    label="Name"
                    changingLabel={this.changingLabel}
                  />
                </div>
              }
            ></FilterPanel>
            <FilterPanel
              label={`Scores ${this.addingItemToLabel(
                this.state.scoreFrom,
                this.state.scoreTo
              )}`}
              content={
                <div className={classes.formRow}>
                  <TextInput
                    id={"score"}
                    label="From"
                    handleChange={this.handleChange}
                    error={this.state.scoreTouch && !this.state.scoreFrom}
                    additionalClass={classes.margin}
                  />

                  <TextInput
                    id={"score"}
                    label="To"
                    handleChange={this.handleChange}
                    error={this.state.scoreTouch && !this.state.scoreTo}
                  />
                </div>
              }
            ></FilterPanel>
            <FilterPanel
              label={`Days of membership ${this.addingItemToLabel(
                this.state.membershipFrom,
                this.state.membershipTo
              )}`}
              content={
                <div className={classes.formRow}>
                  <TextInput
                    id={"membership"}
                    label="From"
                    handleChange={this.handleChange}
                    error={
                      this.state.membershipTouch && !this.state.membershipFrom
                    }
                    additionalClass={classes.margin}
                  />

                  <TextInput
                    id={"membership"}
                    label="To"
                    handleChange={this.handleChange}
                    error={
                      this.state.membershipTouch && !this.state.membershipTo
                    }
                  />
                </div>
              }
            ></FilterPanel>
            <FilterPanel
              label={`Number of wins ${this.addingItemToLabel(
                this.state.winsFrom,
                this.state.winsTo
              )}`}
              content={
                <div className={classes.formRow}>
                  <TextInput
                    id={"wins"}
                    label="From"
                    handleChange={this.handleChange}
                    error={this.state.winsTouch && !this.state.winsFrom}
                    additionalClass={classes.margin}
                  />

                  <TextInput
                    id={"wins"}
                    label="To"
                    handleChange={this.handleChange}
                    error={this.state.winsTouch && !this.state.winsTo}
                  />
                </div>
              }
            ></FilterPanel>
          </div>
          <Button
            className={classes.filterButton}
            variant="contained"
            color="primary"
            onClick={(e) => this.handleClick(e)}
          >
            filter
          </Button>
        </form>
      </>
    );
  }
}

export default withStyles(filterStyles)(Filter);
