export const filterStyles = theme => ({
  root: {
    margin: "2em auto",
    [theme.breakpoints.up("md")]: {
      display: "flex",
      position: "relative",
      top: 50
    }
  },
  filterRow: {
    [theme.breakpoints.up("md")]: {
      display: "flex",
      flexDirection: "row",
      position: "absolute",
      zIndex: 10
    }
  },
  formRow: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      margin: "auto"
    }
  },
  filterButton: {
    width: 60,
    height: 35,
    position: "absolute",
    top: 88,
    right: 7,
    [theme.breakpoints.down("sm")]: {
      position: "relative",
      left: "50%",
      transform: "translateX(-50%)",
      top: 10
    }
  },
  margin: {
    marginRight: "10px"
  },
  closeButton: {
    position: "relative",
    left: "90%",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  }
});
