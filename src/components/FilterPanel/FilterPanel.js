import React, { useState } from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useStyles } from "./FilterPanelStyle";
import { ClickAwayListener } from "@material-ui/core";

export default function FilterPanel({ label, content }) {
  const classes = useStyles();

  const [panel, setPanel] = useState(false);

  const handleClick = () => {
    setPanel(!panel);
  };

  return (
    <ClickAwayListener onClickAway={() => setPanel(false)}>
      <div className={classes.root}>
        <ExpansionPanel expanded={panel} onClick={handleClick}>
          <ExpansionPanelSummary
            className={classes.expansionPanel}
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <p className={classes.label}>{label}</p>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>{content}</ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    </ClickAwayListener>
  );
}
