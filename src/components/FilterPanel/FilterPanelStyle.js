import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    margin: "5px",
    width: "calc(100%/4)",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      margin: "15px auto"
    }
  },
  expansionPanel: { height: "70px" },
  label: {
    fontSize: "16px",
    width: "100%",
    maxHeight: "60px",
    overflow: "hidden"
  }
}));
