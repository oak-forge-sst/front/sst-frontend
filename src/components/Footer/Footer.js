import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { useStyles } from "./FooterStyle.js";
import Navigation from "../Navigation/Navigation.js";

export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar className={classes.bottom}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          ></IconButton>
          <Typography variant="h6" className={classes.title}>
            <Navigation
              direction={"row"}
              justify={"center"}
              alignItems={"center"}
            />
          </Typography>
          <p className={classes.copyRight}> Copyright &copy; 2020 Oakfusion</p>
        </Toolbar>
      </AppBar>
    </div>
  );
}
