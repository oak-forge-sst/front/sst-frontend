import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    position: "relative"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  bottom: {
    position: "absolute",
    top: 0
  },
  title: {
    flexGrow: 1
  },
  copyRight: {
  [theme.breakpoints.down('sm')]: {
    fontSize: 14
    }
  },
  

}));
