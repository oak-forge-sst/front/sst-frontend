import React, {useState} from 'react'
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useStyles } from "./ForgotPasswordStyle";

export default function ForgotPassword() {
    const classes = useStyles();
    const [value, setValue] = useState('')
    return (
        <div className={classes.container}>
            <div>Please enter your email address to search for your account.</div>
        <TextField
        value={value}
        autoFocus
        id="loginEmail"
        label="Email Address"
        type="email"
        onChange={e => {
          setValue(e.target.value);
        }}
      />
        <Button
        className={classes.button}
        color="primary"
        variant="contained"
        >
        Reset password
        </Button>
        </div>
    )
}
