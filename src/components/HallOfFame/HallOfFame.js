import React, { useState } from "react";
import Table from "../Table/Table";
import MainContainer from "../MainContainer/MainContainer";
import Filter from "../Filter/Filter";
import { useStyles } from "./HallOfFameStyle.js";
import Button from "@material-ui/core/Button";
import withWidth from "@material-ui/core/withWidth";
import FilterListIcon from "@material-ui/icons/FilterList";

function HallOfFame({ width }) {
  const [showFilter, setShowFilter] = useState(false);
  const [showTable, setShowTable] = useState(true);

  const toggleFilter = () => {
    setShowFilter(true);
    setShowTable(false);
  };

  const toggleTable = () => {
    setShowFilter(false);
    setShowTable(true);
  };

  const classes = useStyles();
  return (
    <MainContainer>
      {width >= "sm" && !showFilter && (
        <Button
          className={classes.button}
          variant="contained"
          color="primary"
          onClick={toggleFilter}
        >
          <FilterListIcon />
        </Button>
      )}
      {(width < "sm" || showFilter) && (
        <Filter
          className={classes.filter}
          showTable={toggleTable}
          showFilter={showFilter}
        />
      )}

      {(width < "sm" || showTable) && <Table />}
    </MainContainer>
  );
}

export default withWidth()(HallOfFame);
