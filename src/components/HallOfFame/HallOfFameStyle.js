import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  button: { float: "right", margin: "20px auto" }
}));
