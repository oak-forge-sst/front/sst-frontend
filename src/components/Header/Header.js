import React, { useCallback } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { useStyles } from "./HeaderStyle.js";
import { useDispatch, useSelector } from "react-redux";
import { drawerToggle } from "../../store/actions/drawer.action";
import { dialogOpen } from "../../store/actions/dialog.action";
import { logoutRequest } from "../../store/actions/sagaRequest.action";
import LoginAndSignupTabs from "../LoginAndSignupTabs/LoginAndSignupTabs";
import Navigation from "../Navigation/Navigation.js";
import { useHistory } from "react-router-dom";

export default function ButtonAppBar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  let history = useHistory();

  const isLoggedIn = useSelector(
    (state) => state.isLoggedInReducer.switchLogout
  );

  const toggleDrawer = useCallback(() => dispatch(drawerToggle()), [dispatch]);
  const openDialog = useCallback(
    () => dispatch(dialogOpen(LoginAndSignupTabs)),
    [dispatch]
  );

  const logout = useCallback(() => dispatch(logoutRequest(history)), [
    dispatch,
  ]);

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.drawerButton}
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            edge="start"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <div className={classes.navigation}>
              <Navigation
                direction={"row"}
                justify={"flex-start"}
                alignItems={"center"}
              />
            </div>
          </Typography>
          {isLoggedIn ? (
            <Button color="inherit" onClick={logout}>
              Log out
            </Button>
          ) : (
            <Button color="inherit" onClick={openDialog}>
              Log in
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
