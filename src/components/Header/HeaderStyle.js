import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  drawerButton: {
    ["@media screen and (min-width:700px)"]: {
      visibility: "hidden"
    }
  },
  navigation: {
    ["@media screen and (max-width:699px)"]: {
      visibility: "hidden"
    }
  },
  loginButton: {
    fontSize: 24,
    color: "inherit"
  }
}));
