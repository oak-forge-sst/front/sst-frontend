import React from "react";
import MainContainer from "../MainContainer/MainContainer";
import { useStyles } from "./HomeContextStyle";
import pic1 from "../../space-pictures/pic1.jpg"
import pic2 from "../../space-pictures/pic2.jpg"
import pic3 from "../../space-pictures/pic3.jpg"
import pic4 from "../../space-pictures/pic4.jpg"
import pic5 from "../../space-pictures/pic5.jpg"
import gameplay from "../../space-pictures/gameplay.jpg"


const HomeContext = () => {
  const classes = useStyles();
  return (
    <MainContainer>
      <div className={classes.wrapper}>
          <div className={classes.sectionOneWrapper}>
            <img className={classes.picture1} src={pic1} alt='Super Star Trek Background'/>
            <p className={classes.text}>WELCOME TO SUPER STAR TREK</p>
          </div>
          <div className={classes.gameplay}>
            <p className={classes.gameplayText}>
              Stwórz swoje imperium w niekończących się przestworzach
            </p>
            <img className={classes.gameplayImg} src={gameplay} alt={'gameplay'}/>
          </div>
          <img className={classes.picture} src={pic2} alt='Super Star Trek Background'/>
          <img className={classes.picture} src={pic3} alt='Super Star Trek Background'/>
          <img className={classes.picture} src={pic4} alt='Super Star Trek Background'/>
          <img className={classes.picture} src={pic5} alt='Super Star Trek Background'/>
      </div>
    </MainContainer>
  );
};

export default HomeContext;
