import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  wrapper: {
    position: 'relative',  
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%'
  }, 
  sectionOneWrapper: {
    width: '100%',
    height: 'calc(100vh - 70px)',
    position: 'relative'
  },
  picture1: {
    width: '100%',
    height: '100%'
  },
  text: {
    position: 'absolute',
    top: '30%',
    zIndex: '2',
    width: '100%',
    textAlign: 'center',
    color: 'white',
    fontSize: '70px',
    fontWeight: 'bold',
    fontFamily: '"Press Start 2P", cursive',
    lineHeight: '100px'
  },
  picture: {
    width: '100%'
  },
  gameplay: {
    background: '#093e63',
    height: '1000px',
    position: 'relative',  
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  gameplayText: {
    color: 'white'
  },
  gameplayImg: {
    maxWidth: '100%',
    maxHeight: '100%'
  }
}));
