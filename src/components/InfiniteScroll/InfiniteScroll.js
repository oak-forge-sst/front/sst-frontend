import React, { Component } from 'react'
import InfiniteScroller from 'react-infinite-scroller';
import { withStyles } from "@material-ui/styles";
import { useStyles } from "./InfiniteScrollStyle";
import CircularProgress from '@material-ui/core/CircularProgress';

class InfiniteScroll extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div ref={(ref) => this.scrollParentRef = ref}>
                <InfiniteScroller 
                        pageStart={0}
                        loadMore={() => {}}
                        hasMore={true || false}
                        loader={<div className="loader" key={0}>
                        <div className={classes.CircularProgress}>
                            <CircularProgress />
                        </div>
                        </div>}
                        useWindow={false}
                        getScrollParent={() => this.scrollParentRef}
                    >
                        {this.props.children}
                </InfiniteScroller>
            </div>
        )
    }
}

export default withStyles(useStyles)(InfiniteScroll);
