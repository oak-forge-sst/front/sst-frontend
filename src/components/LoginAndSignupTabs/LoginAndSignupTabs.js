import React, {useState} from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import LoginForm from "../LoginForm/LoginForm";
import SignupForm from "../SignupForm/SignupForm";
import ForgotPassword from "../ForgotPassword/ForgotPassword"
import { useStyles } from "./LoginAndSignupTabsStyles";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function setControlProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export default function LoginAndSignupTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [showForgotPassword, setShowForgotPassword] = useState(false)

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab label="LOG IN" {...setControlProps(0)} className={classes.tab} />
          <Tab
            label="SIGN UP"
            {...setControlProps(1)}
            className={classes.tab}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0} >
      { showForgotPassword ? <ForgotPassword/> : 
      <LoginForm
        showForgotPassword={showForgotPassword} 
        setShowForgotPassword={setShowForgotPassword}
        />
      }
      </TabPanel>
      <TabPanel value={value} index={1}>
        <SignupForm />
      </TabPanel>
    </div>
  );
}
