import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
    root: {
      [theme.breakpoints.down('xs')]: {
        width: '250px'
      },
      position: "relative",
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      width: "400px",
      height: "450px"
    },
    tab: {
      width: "50%"
    }
  }));