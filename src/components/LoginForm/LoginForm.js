import React, { useState, useCallback } from "react";
import Button from "@material-ui/core/Button";
import { useStyles } from "./LoginFormStyles";
import TextField from "@material-ui/core/TextField";
import { useDispatch } from "react-redux";
import { loginRequest } from "../../store/actions/sagaRequest.action";
import { useHistory } from "react-router-dom";

export default function Login(props) {
  const classes = useStyles();

  const dispatch = useDispatch();
  let history = useHistory();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleClick = () => {
    props.setShowForgotPassword(!props.showForgotPassword);
  };

  const requestLogin = useCallback(
    () => dispatch(loginRequest(email, password, history)),
    [email, password]
  );

  return (
    <div className={classes.container}>
      <TextField
        value={email}
        autoFocus
        id="loginEmail"
        label="Email Address"
        type="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
      />
      <TextField
        value={password}
        id="loginPassword"
        label="Password"
        type="password"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <Button
        color="primary"
        variant="contained"
        onClick={requestLogin}
        className={classes.loginButton}
      >
        Log In
      </Button>
      <Button className={classes.forgotPasswordButton} onClick={handleClick}>
        Forgot password
      </Button>
    </div>
  );
}
