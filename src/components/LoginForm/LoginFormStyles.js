import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
    container: {
        position: "relative",
        display: "flex",
        flexDirection: "column",
        height: "350px"
    },
    loginButton: {
        position: "absolute",
        bottom: "40px",
        width: "100%"
    },
    forgotPasswordButton: {
        position: "absolute",
        width: "100%",
        bottom: "0"
    }

  });