import React from "react";
import { useStyles } from "./MainContainerStyle";

const Content = ({ children }) => {
  const classes = useStyles();

  return <div className={classes.contentDimension}>{children}</div>;
};

export default Content;
