import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  contentDimension: {
    position: "relative",
    minHeight: "100vh",
    height: "100%",
    fontSize: "30px",
    margin: "auto",
    ["@media screen and (max-width:768px)"]: {
      maxWidth: "80vw"
    }
  }
}));
