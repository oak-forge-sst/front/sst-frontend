import React from "react";
import { Link } from "react-router-dom";
import { useStyles } from "./NavigationStyle.js";
import Grid from "@material-ui/core/Grid";
import cx from "classnames";

const Navigation = ({
  direction,
  justify,
  alignItems,
  additionalClass,
  onClick
}) => {
  const classes = useStyles();

  return (
    <Grid className={classes.grid}
      container
      direction={direction}
      justify={justify}
      alignItems={alignItems}
    >
      <Link
        onClick={onClick}
        className={cx(classes.link, {
          [classes.drawerClass]: additionalClass
        })}
        to="/home"
      >
        Home
      </Link>
      <Link
        onClick={onClick}
        className={cx(classes.link, {
          [classes.drawerClass]: additionalClass
        })}
        to="/news"
      >
        News
      </Link>
      <Link
        onClick={onClick}
        className={cx(classes.link, {
          [classes.drawerClass]: additionalClass
        })}
        to="/halloffame"
      >
        Hall of Fame
      </Link>
    </Grid>
  );
};

export default Navigation;
