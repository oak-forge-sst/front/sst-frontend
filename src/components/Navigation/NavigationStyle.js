import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  link: {
    [theme.breakpoints.down('xs')]: {
      fontSize: 15,
      paddingRight: "1em",
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: 20,
      paddingRight: "1em",
    },
    [theme.breakpoints.up('md')]: {
      paddingRight: "2em",
      fontSize: 24
    },
    color: "inherit",
    textDecoration: "none"
    },
    drawerClass: {
      paddingBottom: "1em"
  }
}));
