import React, { useCallback, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./SignupFormStyles";
import { useDispatch } from "react-redux";
import { validation } from "../ValidationAndMessages/Validation";
import { messages } from "../ValidationAndMessages/Messages";
import { signUpRequest } from "../../store/actions/sagaRequest.action";

export default function Signup() {
  const dispatch = useDispatch();

  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [touched, toggleTouched] = useState(false);

  const signupRequest = useCallback(
    () => dispatch(signUpRequest(email, password, repeatPassword)),
    [email, password, repeatPassword]
  );

  const handleClick = () => {
    toggleTouched(true);
    email &&
      password &&
      repeatPassword &&
      signupRequest(email, password, repeatPassword);
  };

  const handleEmailChange = (evt) => {
    validation(evt) ? setEmail(evt.target.value) : setEmail("");
  };

  const handlePasswordChange = (evt) => {
    validation(evt) ? setPassword(evt.target.value) : setPassword("");
  };

  const handleRepeatPasswordChange = (evt) => {
    if (validation(evt) && evt.target.value === password) {
      setRepeatPassword(evt.target.value);
    } else {
      setRepeatPassword("");
    }
  };

  return (
    <div className={classes.container}>
      <TextField
        onChange={handleEmailChange}
        autoFocus
        margin="dense"
        id="signUpEmail"
        label="Email Address"
        type="email"
        fullWidth
        error={touched && !email}
        helperText={touched && !email ? messages.invalidemail : null}
      />
      <TextField
        onChange={handlePasswordChange}
        margin="dense"
        id="signUpPassword"
        label="Password"
        type="password"
        fullWidth
        error={touched && !password}
        helperText={touched && !password ? messages.invalidpassword : null}
      />
      <TextField
        onChange={handleRepeatPasswordChange}
        margin="dense"
        id="signUpRepeatPassword"
        label="Repeat Password"
        type="password"
        fullWidth
        error={touched && password !== repeatPassword}
        helperText={
          touched && password !== repeatPassword && "Passwords don't match"
        }
      />
      <Button
        color="primary"
        variant="contained"
        className={classes.button}
        onClick={handleClick}
      >
        Sign Up
      </Button>
    </div>
  );
}
