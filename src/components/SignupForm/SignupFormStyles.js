import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
    container: {
        position: "relative",
        display: "flex",
        flexDirection: "column",
        height: "350px"
    },
    button: {
        position: "absolute",
        bottom: "0",
        width: "100%"
    }
  });