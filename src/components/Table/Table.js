import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useStyles } from "./TableStyles";
import InfiniteScroll from "../InfiniteScroll/InfiniteScroll";

const players = [
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 },
  { name: "xxx", scores: 150, membership: 330, wins: 4 },
  { name: "Kgggg", scores: 150, membership: 330, wins: 4 },
  { name: "Klingon", scores: 150, membership: 330, wins: 4 }
];

const keys = Object.keys(players[0]);

const headings = {
  name: "Player name",
  scores: "Scores",
  membership: "Days of membership",
  wins: "Number of wins"
};

export default function SimpleTable() {
  const classes = useStyles();

  return (
    <InfiniteScroll>
      <TableContainer className={classes.root} component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Ordinal number</TableCell>
              {keys.map((key, index) => (
                <TableCell key={index}>{headings[key]} </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {players.map((player, index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                {keys.map((key, index) => (
                  <TableCell key={index}>{headings[key]}</TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </InfiniteScroll>
  );
}
