import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 220,
    [theme.breakpoints.down("xs")]: {
      marginTop: 60
    }
  }
}));
