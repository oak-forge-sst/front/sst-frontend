import React from "react";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./TextInputStyle";

const TextInput = ({
  label,
  id,
  error,
  additionalClass,
  handleChange,
  changingLabel
}) => {
  const classes = useStyles();

  return (
    <TextField
      classes={{
        root: additionalClass
      }}
      InputProps={{
        classes: {
          input: classes.input
        }
      }}
      InputLabelProps={{
        classes: {
          outlined: classes.label
        }
      }}
      id={id}
      label={label}
      variant="outlined"
      onChange={
        handleChange
          ? e => handleChange(e.target.value, id, label)
          : e => changingLabel(e.target.value, id, label)
      }
      error={error}
      onClick={e => e.stopPropagation()}
    />
  );
};

export default TextInput;
