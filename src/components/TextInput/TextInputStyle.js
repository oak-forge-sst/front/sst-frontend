import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  input: {
    padding: "5px"
  },
  label: {
    transform: "translate(14px, 7px) scale(1)"
  }
}));
