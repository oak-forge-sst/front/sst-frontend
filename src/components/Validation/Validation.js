const regex = {
  onlyNumber: /^\d+$/
};

export const validation = value => {
  if (regex.onlyNumber.test(value)) {
    return true;
  } else {
    return false;
  }
};
