const validRegex = {
    regexpassword: /^(((?=.*\d)(?=.*[A-Z])).{8,})+$/,
    regexemail: /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
  }
  
  export const validation = (evt) => {
    let regex = validRegex[`regex${evt.target.type}`]

     if (!regex.test(evt.target.value)){
        return false
     }else{
       return true
    }
  }

    // export const validation = (value) => {
        
    //     console.log(value)
    //      if (!validRegex.regexemail.test(value)){
    //         return false
    //      }else{
    //        return true
    //     }
    //   }