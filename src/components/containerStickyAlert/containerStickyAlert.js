import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useSnackbar } from "notistack";

const ContainerStickyAlert = ({ children }) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const alert = useSelector(state => state.alertReducer.alert);

  useEffect(() => {
    if (alert && alert.message) {
      const key = enqueueSnackbar(alert.message, {
        variant: alert.type,
        onClick: () => {
          closeSnackbar(key);
        }
      });
    }
  }, [alert]);

  return children;
};

export default ContainerStickyAlert;
