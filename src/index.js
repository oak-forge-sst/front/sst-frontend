import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import { drawerReducer } from "./store/reducers/drawer.reducer";
import { dialogReducer } from "./store/reducers/dialog.reducer";
import { isLoggedInReducer } from "./store/reducers/isLoggedIn.reducer";
import { BrowserRouter } from "react-router-dom";
import createSagaMiddleware from "redux-saga";
import { alertReducer } from "./store/reducers/stickyAlert.reducer";
import rootSaga from "./sagas/rootSaga";

const rootReducer = combineReducers({
  drawerReducer,
  dialogReducer,
  alertReducer,
  isLoggedInReducer,
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__
    ? compose(
        applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    : applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
