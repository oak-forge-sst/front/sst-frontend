import { call, take, put } from "redux-saga/effects";
import { CONFIRM_REGISTRATION } from "../store/actions/sagaRequest.action";
import { addAlert } from "../store/actions/stickyAlert.action";
import { request } from "../Api/Api";

function confirmRegistrationRequest(payload) {
  const url = "command";
  const body = {
    aggregateId: payload.id,
  };
  const headers = {
    command: "activate-account",
    ContentType: "application/json",
  };
  const method = "POST";
  return request(url, body, headers, method);
}

export function* confirmRegistrationSaga() {
  const { payload } = yield take(CONFIRM_REGISTRATION);

  const { status, body } = yield call(confirmRegistrationRequest, payload);
  if (status === 200) {
    payload.history.push("/halloffame");
  } else {
    const message = body.message.split("_").join(" ");
    yield put(addAlert({ type: "error", message }));
  }
}
