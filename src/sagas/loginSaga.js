import { call, take, put } from "redux-saga/effects";
import { dialogClose } from "../store/actions/dialog.action";
import { addAlert } from "../store/actions/stickyAlert.action";
import { isLoggedIn, LOGIN_REQUEST } from "../store/actions/sagaRequest.action";
import { request } from "../Api/Api";

function loginRequest(payload) {
  const url = "login";
  const body = {
    emailAddress: payload.email,
    password: payload.password,
  };
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };
  const method = "POST";
  return request(url, body, headers, method);
}

export function* loginSaga() {
  const { payload } = yield take(LOGIN_REQUEST);
  const { status } = yield call(loginRequest, payload);
  if (status === 200) {
    payload.history.push("/halloffame");
    yield put(dialogClose());
    yield put(isLoggedIn());
  } else {
    yield put(
      addAlert({ type: "error", message: "Incorrect email or password " })
    );
  }
}
