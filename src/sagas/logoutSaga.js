import { call, take, put } from "redux-saga/effects";
import { addAlert } from "../store/actions/stickyAlert.action";
import {
  isLoggedIn,
  LOGOUT_REQUEST,
} from "../store/actions/sagaRequest.action";
import { request } from "../Api/Api";

function logoutRequest() {
  const url = "logout";
  const body = {};
  const headers = {
    command: "register-account",
    ContentType: "application/json",
  };
  const method = "POST";
  return request(url, body, headers, method);
}

export function* logoutSaga() {
  const { payload } = yield take(LOGOUT_REQUEST);
  const { status } = yield call(logoutRequest);
  if (status === 200) {
    yield put(isLoggedIn());
    payload.push("/");
  } else {
    yield put(addAlert({ type: "error", message: "You are not logged out" }));
  }
}
