import { all } from "redux-saga/effects";
import { signupSaga } from "./signupSaga";
import { loginSaga } from "./loginSaga";
import { logoutSaga } from "./logoutSaga";
import { confirmRegistrationSaga } from "./confirmRegistrationSaga";

export default function* rootSaga() {
  yield all([
    signupSaga(),
    confirmRegistrationSaga(),
    loginSaga(),
    logoutSaga()
  ]);
}
