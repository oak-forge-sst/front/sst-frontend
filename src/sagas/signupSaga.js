import { call, take, put, all } from "redux-saga/effects";
import {
  SIGNUP_REQUEST,
  signUpSuccess,
  signUpFailure,
} from "../store/actions/sagaRequest.action";
import { addAlert } from "../store/actions/stickyAlert.action";
import { dialogClose } from "../store/actions/dialog.action";
import { request } from "../Api/Api";

function signUpPostRequest(payload) {
  const url = "command";
  const body = {
    emailAddress: payload.email,
    password: payload.password,
    passwordConfirmation: payload.confirmPassword,
  };
  const headers = {
    command: "register-account",
    ContentType: "application/json",
  };
  const method = "POST";
  return request(url, body, headers, method);
}

export function* signupSaga() {
  const { payload } = yield take(SIGNUP_REQUEST);
  const { status, body } = yield call(signUpPostRequest, payload);

  if (status === 200) {
    yield all([
      put(signUpSuccess(status)),
      put(
        addAlert({
          type: "success",
          message: "Registration completed. Check your email.",
        })
      ),
      put(dialogClose()),
    ]);
  } else {
    const message = body.message.split("_").join(" ");
    yield all([
      put(signUpFailure(status)),
      put(addAlert({ type: "error", message })),
    ]);
  }
}
