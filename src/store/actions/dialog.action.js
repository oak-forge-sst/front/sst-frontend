export const OPEN_DIALOG = "OPEN_DIALOG ";
export const CLOSE_DIALOG = "CLOSE_DIALOG ";

export const dialogOpen = component => {
  return {
    type: OPEN_DIALOG,
    payload: component
  };
};
export const dialogClose = () => {
  return {
    type: CLOSE_DIALOG
  };
};
