export const CONFIRM_REGISTRATION = "CONFIRM_REGISTRATION";
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_FAILURE = "SIGNUP_FAILURE";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const IS_LOGGED_IN = "IS_LOGGED_IN";

export const confirmRegistration = (id, history) => {
  return {
    type: CONFIRM_REGISTRATION,
    payload: {
      id,
      history,
    },
  };
};

export const loginRequest = (email, password, history) => {
  return {
    type: LOGIN_REQUEST,
    payload: { email, password, history },
  };
};

export const logoutRequest = (history) => {
  return {
    type: LOGOUT_REQUEST,
    payload: history,
  };
};

export const signUpRequest = (email, password, confirmPassword) => {
  return {
    type: SIGNUP_REQUEST,
    payload: { email, password, confirmPassword },
  };
};

export const signUpFailure = (error) => {
  return {
    type: SIGNUP_FAILURE,
    payload: error,
  };
};

export const signUpSuccess = (data) => {
  return {
    type: SIGNUP_SUCCESS,
    payload: data,
  };
};

export const isLoggedIn = () => {
  return {
    type: IS_LOGGED_IN,
  };
};
