export const ADD_ALERT = "ADD_ALERT";

export const addAlert = alert => {
  return {
    type: ADD_ALERT,
    payload: { alert }
  };
};
