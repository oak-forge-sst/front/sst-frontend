import { OPEN_DIALOG } from "../actions/dialog.action";
import { CLOSE_DIALOG } from "../actions/dialog.action";

const initialState = {
  isOpen: false,
  component: null
};

export const dialogReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_DIALOG:
      return {
        ...state,
        isOpen: true,
        component: action.payload
      };
    case CLOSE_DIALOG:
      return {
        ...state,
        isOpen: false
      };
    default:
      return state;
  }
};
