import {TOGGLE_DRAWER } from '../actions/drawer.action'
export const drawerReducer = (state = false, action) => {
    switch(action.type){
        case TOGGLE_DRAWER:
            return !state
        default:
            return state    
    }
}