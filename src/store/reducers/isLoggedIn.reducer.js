import { IS_LOGGED_IN } from "../actions/sagaRequest.action";

const initialState = {
  switchLogout: false,
};

export const isLoggedInReducer = (state = initialState, action) => {
  switch (action.type) {
    case IS_LOGGED_IN:
      return {
        ...state,
        switchLogout: !state.switchLogout,
      };
    default:
      return state;
  }
};
